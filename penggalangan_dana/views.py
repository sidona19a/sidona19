from django.http import response
from django.shortcuts import render
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def index(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona")
    return render(request,'index.html')

def add_pd(request):

    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona")
    cursor.execute("select nama_kategori from kategori_pd")
    list_kategori = namedtuplefetchall(cursor)
    response['categories'] = list_kategori

    return render(request,'form_addpd.html',response)

def form_kesehatan(request):
    return render(request,'form_kesehatan.html')

def form_rumah_ibadah(request):
    return render(request,'form_rumah_ibadah.html')

def get_input_pd(request):
    input = request.POST


def form_pd(request):

    return render(request,'form_pd.html')

def form_pd_kes(request):
    

    if request.method == 'POST':

        response = {}
        nik = request.POST['nik']
        response['nik'] = nik
        nama = request.POST['nama']
        response['nama'] = nama
        tanggallahir = request.POST['tanggal_lahir']
        response['tanggal_lahir'] = tanggallahir
        alamat = request.POST['alamat']
        response['alamat'] = alamat
        pekerjaan = request.POST['pekerjaan']
        response['pekerjaan'] = pekerjaan

        cursor = connection.cursor()
        cursor.execute("set search_path to sidona")
        cursor.execute("INSERT INTO PASIEN VALUES(%s, %s, %s, %s, %s)", [nik, nama, tanggallahir, alamat, pekerjaan])

        cursor.execute("select * from pasien where nik = %s", [nik])
        # pasien = cursor.fetchone()[0]
        pasien = namedtuplefetchall(cursor)
        response['kesehatan'] = pasien

        # cursor2 = connection.cursor()
        # cursor2.execute("set search_path to sidona")
        # cursor2.execute("")

        # cursor.execute("select id from penggalangan_dana_pd order by id asc")
        # id_pd_view = namedtuplefetchall(cursor)

    return render(request,'form_pd_kesehatan.html', response)

def form_pd_rumahibadah(request):
    

    if request.method == 'POST':

        response = {}
        no_serti = request.POST['no_serti']
        response['no_serti'] = no_serti
        nama_ri = request.POST['nama-ri']
        # response['nama-ri'] = nama_ri
        alamat = request.POST['alamat']
        # response['alamat'] = alamat
        jenis = request.POST['jenis']
        # response['jenis'] = jenis

        cursor = connection.cursor()
        cursor.execute("set search_path to sidona")
        cursor.execute("INSERT INTO rumah_ibadah VALUES(%s, %s, %s, %s)", [no_serti, nama_ri, alamat, jenis])

        cursor.execute("select * from rumah_ibadah where nosertifikat = %s", [no_serti])
        rumah_ibadah = namedtuplefetchall(cursor)
        response['rumah_ibadah'] = rumah_ibadah

        cursor2 = connection.cursor()
        cursor2.execute("set search_path to sidona")
        cursor2.execute("select * from kategori_aktivitas_pd_rumah_ibadah")
        list_kategori_ri = namedtuplefetchall(cursor2)
        response['kategori_ri'] = list_kategori_ri

    return render(request,'form_pd_rumahibadah.html', response)

def validateNIK(nik):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona")
    cursor.execute("select * from pasien")
    list_nik = namedtuplefetchall(cursor)


def baru_kesehatan(request):
    return render(request,'baru_kesehatan.html')

def tidak_baru_kesehatan(request):
    return render(request,'tidak_baru_kesehatan.html')

def baru_rumah_ibadah(request):
    return render(request,'baru_rumah_ibadah.html')

def tidak_baru_rumah_ibadah(request):
    return render(request,'tidak_baru_rumah_ibadah.html')

def daftar_pd_pribadi(request):
    return render(request,'daftar_pd_pribadi.html')

def detail_pd(request):
    return render(request,'detail_pd.html')