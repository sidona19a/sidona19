from django.urls import path
from .views import form_pd_rumahibadah, index, add_pd, form_kesehatan, form_rumah_ibadah, form_pd, baru_kesehatan, tidak_baru_kesehatan, baru_rumah_ibadah, tidak_baru_rumah_ibadah, daftar_pd_pribadi, detail_pd, form_pd_kes, form_pd_rumahibadah

app_name = "penggalangan_dana"

urlpatterns = [
    path('',index, name='index'),
    path('add-pd/', add_pd, name='add_pd'),
    path('form-kesehatan/', form_kesehatan, name='form_kesehatan'),
    path('form-rumah_ibadah/', form_rumah_ibadah, name='form_rumah_ibadah'),
    path('form-pd/', form_pd, name='form_pd'),
    path('form-pd-kes/', form_pd_kes, name='form_pd_kes'),
    path('form-pd-rumahibadah/', form_pd_rumahibadah, name='form_pd_rumahibadah'),
    path('form-kesehatan/baru/', baru_kesehatan, name='baru_kesehatan'),
    path('form-kesehatan/tidak-baru/', tidak_baru_kesehatan, name='tidak_baru_kesehatan'),
    path('form-rumah_ibadah/baru/', baru_rumah_ibadah, name='baru_rumah_ibadah'),
    path('form-rumah_ibadah/tidak-baru/', tidak_baru_rumah_ibadah, name='tidak_baru_rumah_ibadah'),
    path('form-pd/daftar-pd-pribadi/', daftar_pd_pribadi, name='daftar_pd_pribadi'),
    path('detail-pd', detail_pd, name='detail_pd'),
]