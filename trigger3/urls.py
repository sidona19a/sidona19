from django.urls import path

from . import views

app_name = 'trigger3'

urlpatterns = [
    path('read_penggalangan_admin', views.read_penggalangan_admin, name='read_penggalangan_admin'),
    path('read_penggalangan_admin/<str:id>', views.create_catatan_pengajuan, name='create_catatan_pengajuan'),
    path('read_penggalangan_user', views.read_penggalangan_user, name='read_penggalangan_user'),
    path('read_penggalangan_user/<str:id>', views.update_penggalangan, name='update_penggalangan'),
    path('read_catatan_pengajuan', views.read_catatan_pengajuan, name='read_catatan_pengajuan'),
    path('create_kategori', views.create_kategori, name='create_kategori'),
    path('read_kategori', views.read_kategori, name='read_kategori'),
    path('read_kategori/<str:id>', views.update_kategori, name='update_kategori'),
]
