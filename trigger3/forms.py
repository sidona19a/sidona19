from django import forms


class UpdatePDForm(forms.Form):
    id = forms.CharField(label="ID", max_length=100)
    judul = forms.CharField(label="Judul", max_length=100)
    deskripsi = forms.CharField(label="Deskripsi", max_length=100)
    kota = forms.CharField(label="Kota", max_length=100)
    provinsi = forms.CharField(label="Provinsi", max_length=100)
    deadline = forms.DateField()
    jumlah_target_dana = forms.CharField(label="Jumlah Target Dana", max_length=100)
    berkas_penggalangan_dana = forms.CharField(label="Berkas Penggalangan Dana", max_length=100)


class UpdatePDKesehatanForm(forms.Form):
    id = forms.CharField(label="ID", max_length=100)
    judul = forms.CharField(label="Judul", max_length=100)
    deskripsi = forms.CharField(label="Deskripsi", max_length=100)
    kota = forms.CharField(label="Kota", max_length=100)
    provinsi = forms.CharField(label="Provinsi", max_length=100)
    deadline = forms.DateField()
    jumlah_target_dana = forms.CharField(label="Jumlah Target Dana", max_length=100)
    nik_pasien = forms.CharField(label="NIK Pasien", max_length=100)
    nama_pasien = forms.CharField(label="Nama Pasien", max_length=100)
    penyakit_utama = forms.CharField(label="Penyakit Utama", max_length=100)
    komorbid = forms.CharField(label="Komorbid", max_length=100)
    berkas_penggalangan_dana = forms.CharField(label="Berkas Penggalangan Dana", max_length=100)


class UpdatePDRumahIbadahForm(forms.Form):
    id = forms.CharField(label="ID", max_length=100)
    judul = forms.CharField(label="Judul", max_length=100)
    deskripsi = forms.CharField(label="Deskripsi", max_length=100)
    kota = forms.CharField(label="Kota", max_length=100)
    provinsi = forms.CharField(label="Provinsi", max_length=100)
    deadline = forms.DateField()
    jumlah_target_dana = forms.CharField(label="Jumlah Target Dana", max_length=100)
    nomor_sertifikat_rumah_ibadah = forms.CharField(label="Nomor Sertifikat Rumah Ibadah", max_length=100)
    kategori_aktivitas = forms.CharField(label="Kategori Aktivitas", max_length=100)
    berkas_penggalangan_dana = forms.CharField(label="Berkas Penggalangan Dana", max_length=100)


class CreateCatatanPengajuanForm(forms.Form):
    catatan = forms.CharField(label="Catatan Pengajuan", max_length=100)


class CreateKategoriForm(forms.Form):
    kategori = forms.CharField(label="Kategori", max_length=100)


class UpdateKategoriForm(forms.Form):
    kategori = forms.CharField(label="Kategori", max_length=100)