from django.shortcuts import render
from .forms import *
from django.db import connection
from collections import namedtuple
from time import gmtime, strftime


# https://docs.djangoproject.com/en/3.0/topics/db/sql/
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


# RU Penggalangan Dana

def read_penggalangan_admin(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona;")

    cursor.execute("select penggalangan_dana_pd.*, nama_kategori from penggalangan_dana_pd inner join kategori_pd on id_kategori = kategori_pd.id;")

    result = namedtuplefetchall(cursor)
    print(result)

    return render(request, "read_penggalangan_dana_admin.html", {"penggalangan_dana": result})


def read_penggalangan_user(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona;")

    cursor.execute("select penggalangan_dana_pd.*, nama_kategori from penggalangan_dana_pd inner join kategori_pd on id_kategori = kategori_pd.id;")

    result = namedtuplefetchall(cursor)

    return render(request, "read_penggalangan_dana_user.html", {"penggalangan_dana": result})


def update_penggalangan(request, id):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona;")

    cursor.execute(f"select penggalangan_dana_pd.*, nama_kategori, admin.nama as nama_admin from penggalangan_dana_pd "
                   f"inner join kategori_pd on id_kategori = kategori_pd.id "
                   f"inner join admin on admin.email = email_admin "
                   f"where penggalangan_dana_pd.id = '{id}';")
    penggalangan_dana = namedtuplefetchall(cursor)
    kategori = penggalangan_dana[0].id

    if request.method == "POST":
        pass

    template = "update_penggalangan_dana.html"
    if kategori == "kesehatan":
        template = "update_penggalangan_dana_kesehatan.html"
    elif kategori == "rumah ibadah":
        template = "update_penggalangan_dana_rumahibadah.html"

    return render(request, template, {"penggalangan_dana": penggalangan_dana})


# CR Catatan Pengajuan

def create_catatan_pengajuan(request, id):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona;")

    if request.method == "POST":
        timestamp = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        informasi = request.POST.get("informasi")

        if informasi is not None:
            cursor.execute(f"insert into catatan_pengajuan values ({id}, to_timestamp('{timestamp}', 'YYYY-MM-DD HH-MI-SS'), '{informasi}');")

            cursor.execute(f"update penggalangan_dana_pd set status_verifikasi = 'Terverifikasi' where id = '{id}';")

            today = strftime("%Y-%m-%d %H:%M:%S", gmtime())
            cursor.execute(f"update penggalangan_dana_pd "
                           f"set tanggal_aktif_awal = to_timestamp('{today}', 'YYYY-MM-DD HH-MI-SS') "
                           f"where id = '{id}';")

    return render(request, "create_catatan_pengajuan.html")


def read_catatan_pengajuan(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona;")

    cursor.execute("select * from penggalangan_dana_pd, catatan_pengajuan where id = idpd;")

    result = namedtuplefetchall(cursor)
    return render(request, "read_catatan_pengajuan.html", {"catatan_pengajuan": result})


# CRU Kategori PD

def create_kategori(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona;")

    cursor.execute("select max(id) from kategori_pd")
    id = namedtuplefetchall(cursor)[0][0]

    id = str(int(id) + 1)

    if request.method == "POST":
        nama_kategori = request.POST.get("nama_kategori")

        if nama_kategori is not None:
            cursor.execute(f"insert into kategori_pd values ({id}, '{nama_kategori}')")

    return render(request, "create_kategori.html", {"id": id})


def read_kategori(request):
    cursor = connection.cursor()

    cursor.execute("set search_path to sidona;")
    cursor.execute("select * from kategori_pd;")

    result = namedtuplefetchall(cursor)
    print(result)
    return render(request, "read_kategori_pd.html", {"kategori": result})


def update_kategori(request, id):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona;")

    if request.method == "POST":
        nama_kategori = request.POST.get("nama_kategori")

        if nama_kategori is not None:
            cursor.execute(f"update kategori_pd set nama_kategori = '{nama_kategori}' where id = '{id}'")

    return render(request, "update_kategori.html", {"id": id})
