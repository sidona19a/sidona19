from django.shortcuts import render
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def index(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona")
    cursor.execute("select k.idpd, pd.judul, pk.penyakit, k.komorbid from komorbid k join penggalangan_dana_pd pd on k.idpd = pd.id join pd_kesehatan pk on pk.idpd = k.idpd")
    list_komorbid = namedtuplefetchall(cursor)
    response = {
        'komorbid':list_komorbid,
    }

    return render(request,'index_k.html', response)
    
def add_form(request):
    return render(request,'add_form.html')

def update_komorbid(request):
    return render(request,'update_komorbid.html')