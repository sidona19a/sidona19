from django.urls import path
from django.urls.resolvers import URLPattern
from .views import index, add_form, update_komorbid

app_name = "penyakit_komorbid"

urlpatterns = [
    path('',index, name='index'),
    path('add-form/',add_form,name='add-form'),
    path('update-komorbid/',update_komorbid,name='update_komorbid'),
]