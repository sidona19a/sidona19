from django.apps import AppConfig


class PenyakitKomorbidConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'penyakit_komorbid'
