from django.urls import path
from .views import *

app_name = "login"

urlpatterns = [
    path('login', index, name='index'),
    path('', home, name='home'),
    path('register2', register2, name='register2'),
    path('register', register, name='register'),
    path('register_admin', register_admin, name='register_admin'),
    path('profile', profile, name='profile'),
    path('home', login,name='home'),
    path('logout', logout,name='logout'),
    

    
]
