from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from .forms import LoginForm

# Create your views here.
# def index(request):
#     try:
#         email = request.session['email']
#         password = request.session['password']
#     except:
#         email = request.POST['email']
#         password = request.POST['password']

#     email = str(email)
#     password = str(password)
#     result = query("select * from penggalang_dana where email='"+email+"' and password='"+password+"'")

def index(request, validasi = None, message=""):
    try:
        email = request.session['email']
        return login(request)
    except KeyError:
   
        if(validasi==False):
            message = "Invalid email or password"
        argument = { 

            'message' : message
        }
        return render(request, 'login.html', argument)
    
def login(request):
    print(request.session)
    try:
        email = request.session['email']
        password = request.session['password']
    except:
        email = request.POST['email']
        password = request.POST['password']

    email = str(email)
    password = str(password)
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona")
    cursor.execute("select * from admin where email='"+email+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil != []):
        request.session['role'] = 'admin'
    else:
        cursor.execute("set search_path to sidona")
        cursor.execute("select * from penggalang_dana where email='"+email+"' and password='"+password+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil != []):
            request.session['role'] = 'penggalang_dana'

        
        
        
  

    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        request.session['email'] = hasil[0].email
        request.session.set_expiry(1800)
       
        argument = {
            'hasil' : hasil,
            
        }
        cursor.close()
        return render(request, "home.html", argument)
    
    
def logout(request):
    request.session.flush()
    request.session.clear_expired()
    return index(request)       

def register2(request):
    role = request.POST["role"]
    
    if(role != "ADMIN"):
    
        print("TES PD",role)
        return penggalang_dana(request)
    else:
        return admin(request)

def admin(request):
    return render(request, "register_admin.html")

def penggalang_dana(request):
    return render(request, "register_p_dana.html")

def register_admin(request):
    messages=""
    email = request.POST['email']
    password = request.POST['password']
    nama = request.POST['nama']
    nomor_hp = request.POST['nomor_hp']
    id =  query("select id_pegawai from admin order by id_pegawai asc limit 1")
    id_pegawai = str(int(id[0].id_pegawai) + 1)
    
    try:
        query("insert into admin values ('"+email+"','"+password+"' ,'"+nama+"'  ,'"+nomor_hp+"' ,'"+id_pegawai+"' )"  )
        messages="Berhasil, silahkan login"
    except:
        messages="Email sudah pernah terdaftar sebelumnya. Silakan gunakan email yang lain"
    return index(request,message=messages)

    
        

    

def namedtuplefetchall(cursor):
        "Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]


def query(command):
        res=[]
        cursor = connection.cursor()
        cursor.execute("set search_path to sidona")
        cursor.execute(command)
        if(command[:6].lower() == "select"):
            res = namedtuplefetchall(cursor)
        cursor.close()
        
        return res
        
        
        
    
def register(request):
    return render(request, 'register.html')

def home(request):
    return render(request, 'home.html')

def profile(request):
    return render(request, 'profile.html')

