from django.urls import path
from .views import pencairan, tes_sql


urlpatterns = [
    path('pencairan', pencairan, name='pencairan'),
    path('tesdb', tes_sql, name='tes_sql' ),

    
]
