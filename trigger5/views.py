from django.shortcuts import render
from django.db import connection
from collections import namedtuple


# Create your views here.
def pencairan(request):
        return render(request, 'pencairan.html')


def tes_sql(request):
        respon = query("select  * from kategori_pd")
        return render(request, 'tesdb.html', respon)


def namedtuplefetchall(cursor):
        "Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]


def query(command):
        respon = {}
        cursor = connection.cursor()
        cursor.execute("set search_path to sidona")
        cursor.execute(command)
        res = namedtuplefetchall(cursor)
        respon['res'] = res
        return respon
        
        
