from django.shortcuts import render
from django.db import connection
from collections import namedtuple

# Create your views here.
# def wishlist_index(request) :
#     return render(request, 'wishlist_main.html')

def wishlist_index(request) :
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona")
    cursor.execute("""
        select w.idpd, p.judul, k.nama_kategori from wishlist_donasi w
            join penggalangan_dana_pd p on w.idpd = p.id
            join kategori_pd k on p.id_kategori = k.id""")
    wishlist = namedtuplefetchall(cursor)
    jumlah_wishlist = len(wishlist)
    response = {
        'wishlist' : wishlist,
        'jumlah' : jumlah_wishlist,
    }
    return render(request, 'wishlist_main.html', response)

# def wishlist_profile(request) :
#     return render(request, 'wishlist_profile.html')

def wishlist_profile(request) :
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona")
    cursor.execute("""
        select w.idpd, p.judul, k.nama_kategori from wishlist_donasi w
            join penggalangan_dana_pd p on w.idpd = p.id
            join kategori_pd k on p.id_kategori = k.id""")
    wishlist = namedtuplefetchall(cursor)
    jumlah_wishlist = len(wishlist)
    response = {
        'wishlist' : wishlist,
        'jumlah' : jumlah_wishlist,
    }
    return render(request, 'wishlist_profile.html', response)

def namedtuplefetchall(cursor):
        "Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]