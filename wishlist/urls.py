from django.urls import path
from .views import wishlist_index, wishlist_profile

app_name = "wishlist"

urlpatterns = [
    path('', wishlist_index, name='index'),
    path('profile', wishlist_profile, name='profile'),
]