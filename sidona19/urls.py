"""sidona19 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
import trigger3.urls as trigger3

urlpatterns = [
    path('admin/', admin.site.urls),
    path('trigger3/', include(trigger3)),
    path('', include('login.urls')),
     path('trigger5/', include('trigger5.urls')),
    path('wishlist/', include('wishlist.urls')),
    path('donasi/', include('donasi.urls')),
    path('penggalangan_dana/', include('penggalangan_dana.urls')),
    path('penyakit_komorbid/',include('penyakit_komorbid.urls')),
]
