from django.urls import path
from .views import donasi_index, donasi_detail, add_donasi, upload_bukti

app_name = "donasi"

urlpatterns = [
    path('', donasi_index, name='index'),
    path('add', add_donasi, name='form'),
    path('detail', donasi_detail, name='detail'),
    path('bukti', upload_bukti, name='bukti'),
]