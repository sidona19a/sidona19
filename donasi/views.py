from django.http import response
from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple

# Create your views here.
def donasi_index(request):
    # email = request.session['email']
    cursor = connection.cursor()
    cursor.execute("set search_path to sidona")
    # cursor.execute("select * from donasi d join status_pembayaran s on d.idstatuspembayaran = s.id")
    cursor.execute('''
        select d.idpd, p.judul, k.nama_kategori, s.status from donasi d
        join status_pembayaran s on d.idstatuspembayaran = s.id
        join penggalangan_dana_pd p on d.idpd = p.id
        join kategori_pd k on p.id_kategori = k.id''')
    donasi = namedtuplefetchall(cursor)
    context = {
        'donasi' : donasi,
    }
    return render(request, 'donasi_index.html', context)
    
    # try:
    #     email = str(request.session['email'])
    # except Exception as e:
    #     return redirect('/')

    # cursor = connection.cursor()
    # cursor.execute("set search_path to sidona")
    # # cursor.execute("select * from donasi d join status_pembayaran s on d.idstatuspembayaran = s.id")
    # cursor.execute("""
    #     select d.email, d.idpd, p.judul, k.nama_kategori, s.status from donasi d
    #     join status_pembayaran s on d.idstatuspembayaran = s.id
    #     join penggalangan_dana_pd p on d.idpd = p.id
    #     join kategori_pd k on p.id_kategori = k.id
    #     where d.email = '""" + email + "'")
    # donasi = namedtuplefetchall(cursor)
    # context = {
    #     'donasi' : donasi,
    # }
    # return render(request, 'donasi_index.html', context)

def add_donasi(request):
    return render(request, 'donasi_form.html')

def upload_bukti(request):
    return render(request, 'donasi_form_bukti.html')

def donasi_detail(request):
    return render(request, 'donasi_detail.html')

def namedtuplefetchall(cursor):
        "Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]